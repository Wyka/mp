import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        StatsArray: [],
        UserVAC: [],
        UserInfoArray: []
    },
    getters: {
        getStatsArray(state) {
            return state.StatsArray;
        },
        getUserVAC(state) {
            return state.UserVAC;
        },
        getUserInfoArray(state) {
            return state.UserInfoArray;
        },
    },
    mutations: {
        setStatsArray(state, StatsArray) {
            state.StatsArray = StatsArray;
        },
        setUserVAC(state, UserVAC) {
            state.UserVAC = UserVAC;
        },
        setUserInfoArray(state, UserInfoArray) {
            state.UserInfoArray = UserInfoArray;
        },
    },
    actions: {

    }
})
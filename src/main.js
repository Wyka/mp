import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import BootstrapVue from 'bootstrap-vue'
import VueFeather from 'vue-feather'
import axios from 'axios'
import Vuebar from 'vuebar'
import Vuetify from 'vuetify'
import VTooltip from 'v-tooltip'
import VueApexCharts from 'vue-apexcharts'


// CSS
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'vuetify/dist/vuetify.min.css'
import './assets/css/style.css'

Vue.component('apexchart', VueApexCharts)
Vue.use(axios)
Vue.use(Vuebar)
Vue.use(Vuetify)
Vue.use(VTooltip)
Vue.use(VueFeather)
Vue.use(BootstrapVue)
Vue.use(require('vue-moment'))
Vue.use(Vuetify, {
    iconfont: 'md'
})
Vue.prototype.$axios = axios
Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: function(h) { return h(App) }
}).$mount('#app')
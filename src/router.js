import Vue from 'vue'
import Router from 'vue-router'
import Index from './views/Index.vue'
import Stats from './views/Stats.vue'
import Music from './views/Music.vue'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [{
            path: '/',
            name: 'Strona główna',
            component: Index
        },
        {
            path: '/stats',
            name: 'Statystyki',
            component: Stats
        },
        {
            path: '/music',
            name: 'Muzyka',
            component: Music
        },
        {
            path: '/about',
            name: 'About',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: function() {
                return import ( /* webpackChunkName: "about" */ './views/About.vue')
            }
        }
    ]
})